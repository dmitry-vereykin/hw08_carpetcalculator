/**
 * Created by Dmitry on 6/7/2015.
 */
public class RoomDimension {

    private double length;
    private double width;

    public RoomDimension(double len, double w){
        length = len;
        width = w;
    }

    public double getArea(){
        return length * width;
    }

    public String toString() {
        String str = "Room Dimensions:" + "\nLength: " + length + "  Width: " + width;
        return str;
    }
}
